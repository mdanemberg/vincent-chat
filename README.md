## Instalar pacotes
```sh
npm install
```

## Rodar local
```sh
npm start
```

## Build
```sh
npm run build
```

## Colocar no site
Para colocar no site precisa rodar o build primeiro.
Depois de terminar o build basta copiar o js de `/build/static/js/main.$hash.js`.
Nesse arquivo vai estar tudo o que o chat precisa para funcionar.
Depois de adicionar esse arquivo na página de vocês basta criar um elemento HTML que vai ser onde o chat vai ser montado.
Esse elemento deve ser assim: `<div id="chat-vincent"></div>`.