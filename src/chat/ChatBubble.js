import React, { Component } from 'react';

const styles = {
  chatbubble: {
    borderRadius: 20,
    clear: 'both',
    marginTop: 1,
    marginRight: 'auto',
    marginBottom: 1,
    marginLeft: 'auto',
    maxWidth: 425,
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 14,
    paddingRight: 14,
    width: '-webkit-fit-content'
  },
  chatbubbleOrientationNormal: {
    float: 'right'
  },
  recipientChatbubble : {
    backgroundColor: '#f1f0f0',
    p: {
      color: '#000',
      fontSize: 16,
      fontWeight: '300',
      margin: 0,
    }
  },
  quickReplyBubble: {
    border: '2px solid #ddd',
    textDecoration: 'none',
    p: {
      color: '#000',
      fontSize: 16,
      fontWeight: '300',
      margin: 0      
    }
  },
  recipientChatbubbleOrientationNormal: {
    float: 'left'
  },
  p: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: '300',
    margin: 0,
  }
};

const debuggerStyle = {
  fontSize: 12,
  color: '#777',
  marginRight: 15
};

const senderStyle = {
  ...debuggerStyle,
  color: '#FFFFFF',
  marginTop: 10
};

export default class ChatBubble extends Component {
  constructor(props) {
    super();
    this.state = {
      message: props.message,
      bubbleStyles: props.bubbleStyles?
        {
          text: props.bubbleStyles.text?props.bubbleStyles.text:{},
          chatbubble: props.bubbleStyles.chatbubble?props.bubbleStyles.chatbubble:{},
          userBubble: props.bubbleStyles.userBubble?props.bubbleStyles.userBubble:{}
        }
        : {text:{},chatbubble:{}}
    };
  }

  componentDidMount() {}

  // IPR
  _parse_for_styles(message) {
    if (typeof(message) === "string") {
      var bolded_start = message.search(/__(\w+\s?)+__/);
      var bolded_end = message.slice(bolded_start+2).search(/__/);
      var bolded = message.slice(bolded_start + 2, bolded_start + bolded_end + 2);
      // Render text
      if (bolded_start !== -1 && bolded_end !== -1) {
        return (
          <span>
            {this._parse_for_styles(message.slice(0, bolded_start))}
            <strong>{bolded}</strong>
            {this._parse_for_styles(message.slice(bolded_start + bolded_end + 4))}
          </span>
        );
      }
      else {
        return <span>{message}</span>;
      }
    }
    return message;
  }

  // IPR
  _parse_for_links(message) {
    var i, j;

    // eslint-disable-next-line
    if (message.search(/<a href=/) !== -1 && (i = message.search(/<a href=/)) && (j = message.search(/a>/))) {
      
      return (<p>
        {message.slice(0, i)}
        <a target="_blank"
          href={message.slice(i+9, message.search(/'>|">/))}>
          {message.slice(message.search(/'>|">/)+2, j-2)}
        </a>
        {message.slice(j+2)}</p>
      );
    }
    else {
      return <p>{message}</p>;
    }
  }

  handleQuickReply(message) {
    this.props.handleQuickReply(message);
  }

  render() {
    var debuggerMessage = null;
    var senderName = null;

    if (this.state.message.score && this.state.message.tokens) {
      debuggerMessage = 
          <div>
            <br/>  
            <span style={debuggerStyle} ><strong>Intenção:</strong> {this.state.message.intent_text}</span>
            <span style={debuggerStyle} ><strong>Score:</strong> {this.state.message.score}</span>
            <span style={debuggerStyle} ><strong>Tokens:</strong> {this.state.message.tokens.map((t) => t + ", ")}</span>
          </div>;
    }

    if (this.state.message.senderName){
      senderName =
        <div style={senderStyle}>
          <span><strong>Enviado por: </strong> {this.state.message.senderName}</span>
        </div>;
    }


    if (this.props.message.id === 1) {
        return (
          <div style={Object.assign({},
            styles.chatbubble,
            styles.recipientChatbubble,
            (this.props.bubblesCentered?{}:styles.recipientChatbubbleOrientationNormal),
            this.state.bubbleStyles.chatbubble
          )}>
            <p style={Object.assign({},styles.recipientChatbubble.p, this.state.bubbleStyles.text)}>{this.props.message.message}</p>
            
            {debuggerMessage}
            {senderName}
          </div>
        );
    } else if (this.props.message.id === 2) {
        return (
          <div>
            {this.props.message.message.map((message) => 
              <a href="javascript:;" onClick={this.handleQuickReply.bind(this, message)} data-test="123" key={Math.random().toString(36)} style={Object.assign({},
                styles.chatbubble,
                styles.quickReplyBubble,
                (this.props.bubblesCentered?{}:styles.recipientChatbubbleOrientationNormal),
                this.state.bubbleStyles.chatbubble
              )}>
                  <p style={Object.assign({},styles.quickReplyBubble.p, this.state.bubbleStyles.text)}>{message}</p>
              </a>
            )}
          </div>
        );
    } else {
      return (
        <div style={Object.assign({},
          {backgroundColor: '#11b17b'},
          styles.chatbubble,
          (this.props.bubblesCentered?{}:styles.chatbubbleOrientationNormal),
          this.state.bubbleStyles.chatbubble,
          this.state.bubbleStyles.userBubble
        )}>
          <p style={Object.assign({},styles.p, this.state.bubbleStyles.text)}>{this.props.message.message}</p>
          {senderName}
        </div>
      );
    }
  }
}