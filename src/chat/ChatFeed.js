import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import ChatBubble from './ChatBubble';
import Message from './Message';

const styles = {
  chatPanel: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1
  },
  chatHistory: {
    overflow: 'scroll'
  },
  chatbubbleWrapper: {
    marginTop: 10,
    marginBottom: 10,
    overflow: 'auto',
    position: 'relative'
  },
  img: {
    borderRadius: 100,
    bottom: 0,
    left: 0,
    position: 'absolute',
    width: 36,
    zIndex: 100,
  }
};

class ChatFeed extends Component {

  constructor(props) {
    super(props);
    this.state = {
      messages: props.messages || [],
    };
  }

  _scrollToBottom() {
    const {chat} = this.refs;
    const scrollHeight = chat.scrollHeight;
    const height = chat.clientHeight;
    const maxScrollTop = scrollHeight - height;
    ReactDOM.findDOMNode(chat).scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
  }

  /**
    * Parses and collects messages of one type to be grouped together.
    *
    * @param {messages} - a list of Message objects
    * @param {index} - the index of the end of the message group
    * @param {type} - the type of group (user or recipient)
    * @return {message_nodes} - a JSX wrapped group of messages
    */
  _renderGroup(messages, index, id) {
    var group = [];

    for (var i = index; messages[i]?(messages[i].id === id):false; i--) {
      group.push(messages[i]);
    }

    var message_nodes = group.reverse().map((curr, index) => {
      return <ChatBubble
                key={Math.random().toString(36)}
                handleQuickReply={this.props.handleQuickReply}
                message={curr}
                bubblesCentered={this.props.bubblesCentered?true:false}
                bubbleStyles={this.props.bubbleStyles}/>
    });
    return (
      <div key={Math.random().toString(36)} style={styles.chatbubbleWrapper}>
        {message_nodes}
      </div>
    );
  }

  /**
    * Determines what type of message/messages to render.
    *
    * @param {messages} - a list of message objects
    * @return {message_nodes} - a list of message JSX objects to be rendered in
    *   our UI.
    */
  _renderMessages(messages) {
    var message_nodes = messages.map((curr, index) => {

      // Find diff in message type or no more messages
      if (
        (messages[index+1]?false:true) ||
        (messages[index+1].id !== curr.id)
      ) {
        return this._renderGroup(messages, index, curr.id);
      }

      return null;
    });

    // Other end is typing...
    if (this.props.isTyping) {
      message_nodes.push(
        <div key={Math.random().toString(36)} style={Object.assign({}, styles.recipient, styles.chatbubbleWrapper)}>
          <ChatBubble message={new Message({id:1, message:"..."})} bubbleStyles={this.props.bubbleStyles?this.props.bubbleStyles:{}}/>
        </div>
      )
    }

    // return nodes
    return message_nodes

  }

  /**
  * render : renders our chatfeed
  */
  render() {
    window.setTimeout(() => {
      this._scrollToBottom()
    },10)
    
    const panelStyles = this.props.panelStyles || {};
    const chatHistoryStyles = this.props.chatHistoryStyles || {};

    return (
      <div id="chat-panel" style={{...styles.chatPanel, ...panelStyles}}>
        <div ref="chat" className="chat-history" style={{...styles.chatHistory, ...chatHistoryStyles}}>
          <div className="chat-messages" >
            {this._renderMessages(this.props.messages)}
          </div>
        </div>
      </div>
    )
  }
}

ChatFeed.propTypes =  {
  isTyping: React.PropTypes.bool,
  onSubmit: React.PropTypes.func,
  handleChange: React.PropTypes.func,
  handleQuickReply: React.PropTypes.func,
  bubblesCentered: React.PropTypes.bool,
  bubbleStyles: React.PropTypes.object,
  panelStyles: React.PropTypes.object,
  messages: React.PropTypes.array.isRequired
}

export default ChatFeed; 