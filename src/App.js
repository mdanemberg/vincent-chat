import React, { Component } from 'react';
import ChatFeed from './chat/ChatFeed.js';
import Message from './chat/Message.js';
import axios from 'axios';

const styles = {
  body: {
    fontFamily: 'sans-serif'
  },
  chatInput: {
    flex: 1
  },
  inputStyle: {
    border: 'none',
    borderTopWidth: '1',
    borderTopStyle: 'solid',
    borderTopColor: '#ddd',
    fontSize: '16',
    outline: 'none',
    padding: '30',
    width: '100%'
  }
};

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      messages: [
        (new Message({ id: 1, message: "Olá! Em alguns segundos vou te mostrar como a Orgânica pode te ajudar." })),
        (new Message({ id: 1, message: "Normalmente nossos clientes nos contratam para resolver alguma das situações abaixo. Qual se encaixa melhor no que você precisa?" })),
        (new Message({ id: 2, message: [
          'Quero vender mais!', 
          'Meu cliente alvo não entende o valor da minha solução', 
          'Quero reduzir custo ou tempo de negociação',
          'Meu cliente alvo não me encontra',
          'Meu time de vendas está ocioso',
          'Nenhuma dessas opções'
        ]})),
      ],
      is_typing: false,
      waiting: false,
      message: '',
      inputPlaceholder: 'Mensagem',
      lead: {},
      flow: {
        current: false,
        next: false
      }
    };
  }

  validateEmail(email) {    
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  validatePhone(phone) {    
    var re = /^(?:(?:\+|00)?(55)\s?)?(?:\(?([1-9][0-9])\)?\s?)?(?:((?:9\d|[2-9])\d{3})\-?(\d{4}))$/;
    return re.test(phone);
  }

  handleChange(event) {
    this.setState({message: event.target.value});
  }

  handleQuickReply(message) {
    this.send(message);
  }

  handleSend(ev) {
    ev.preventDefault();
    if (this.state.waiting) return;

    this.send(this.state.message);
    this.setState({message: ''});
  }

  newMessage(type, data) {
    return (new Message({
      id: type,
      message: data.message.text
    }));
  }

  save() {
    axios.post('https://beta.site321.com.br/api/v2/contents/5575483c1b1406f5', this.state.lead)
      .then((response) => {
        this.setState({lead_id: response.data.item.id});
      })
      .catch((error) => {
        console.log(error);
      });
  }

  edit() {
    axios.put(`https://beta.site321.com.br/api/v2/contents/5575483c1b1406f5/${ this.state.lead_id }`, this.state.lead)
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });   
  }

  send(message) {
    if (this.state.flow.current) {
      this.setState({waiting: true});
      this.state.messages.push((new Message({ id: 0, message: message })));
      this.forceUpdate();
      this.flowControl({intent_text: this.state.flow.current, message: message});
    } else {
      this.state.messages.push((new Message({ id: 0, message: message })));
      this.forceUpdate();
      this.setState({waiting: true});

      this.setState({is_typing: true});
      axios.post('https://api.vincent-ai.com/v1/organicadigital/messages', {text: message})
        .then((response) => {
          this.state.messages.push(this.newMessage(1, response.data));
          this.forceUpdate();
          this.setState({waiting: false});
          this.setState({is_typing: false});
          this.flowControl(response.data);
        })
        .catch((error) => {
          console.log(error);
        });
    }    
  }

  delay(func) {
    this.setState({is_typing: true});
    setTimeout(func, 1500);
  }

  done() {
    this.forceUpdate();
    this.setState({is_typing: false});
    this.setState({waiting: false});
  }

  flowControl(response) {
    const { message, intent_text } = response;
    let skip = false;

    switch (intent_text) {
      case 'vender_mais':
      case 'cliente_nao_entende':
      case 'reduzir_tempo_negociacao':
      case 'cliente_nao_encontra':
      case 'time_ocioso':
        this.setState({
          lead: {
            ...this.state.lead,
            intent: intent_text
          },
          flow: {
            current: 'name',
            next: 'e-mail'
          },
          inputPlaceholder: 'Qual é o seu nome?'
        });        

        this.delay(() => {
          this.state.messages.push((new Message({ id: 1, message: 'Ah, qual o seu nome? Só o primeiro nome mesmo, não precisamos de formalidades. :)' })));          
          this.done();
        });

        break;
      case 'nenhuma_opcao':
        this.setState({
          lead: {
            ...this.state.lead,
            intent: intent_text
          },
          flow: {
            current: 'question',
            next: 'name'
          },
          inputPlaceholder: 'Qual é seu principal problema?'
        });                
        break;
      case 'question':
        this.setState({
          lead: {
            ...this.state.lead,
            question: message
          },
          flow: {
            current: 'name',
            next: 'e-mail'
          },
          inputPlaceholder: 'Qual é o seu nome?'
        });

        this.delay(() => {
          this.state.messages.push((new Message({ id: 1, message: 'Ah, qual o seu nome? Só o primeiro nome mesmo, não precisamos de formalidades. :)' })));          
          
          // Descomentar para funcionar o put
          // this.edit();
          this.done();
        });
        break;
      case 'name':
        this.setState({
          lead: {
            ...this.state.lead,
            name: message
          },
          flow: {
            current: this.state.flow.next,
            next: 'phone'
          },
          inputPlaceholder: 'Me conte seu e-mail...'
        });

        this.delay(() => {
          this.state.messages.push((new Message({ id: 1, message: `Olá ${ message }! Acredito que você já tenha percebido, mas eu sou um robô!` })));
          this.state.messages.push((new Message({ id: 1, message: 'Você sabia que uma pesquisa recente diz que mais que 80% das pessoas preferem falar com robôs ao invés de atendentes humanos? :) Enfim, isso não vem ao caso agora.' })));
          this.state.messages.push((new Message({ id: 1, message: 'Você poderia me deixar o seu e-mail? Preciso dele para não perdermos contato e para que possamos dar os próximos passos.' })));
          this.done();
        });
        break;
      case 'e-mail':
        if (this.validateEmail(message)) {
          this.setState({
            lead: {
              ...this.state.lead,
              email: message
            },
            flow: {
              current: this.state.flow.next,
              next: 'finish'
            },
            inputPlaceholder: 'Telefone?'
          });

          this.delay(() => {
            this.state.messages.push((new Message({ id: 1, message: 'Obrigado!' })));
            this.state.messages.push((new Message({ id: 1, message: 'Vou encaminhar seu contato para um humano. OK? Eles ainda são melhores do que eu para essas coisas…' })));
            this.state.messages.push((new Message({ id: 1, message: 'Seria legal ter também o seu telefone. Poderia digitar abaixo?' })));
            this.state.messages.push((new Message({ id: 2, message: ['Não enviar telefone'] })));
            this.done();
            this.save();
          });
        } else {
          this.delay(() => {
            this.state.messages.push((new Message({ id: 1, message: 'Por favor, envie um e-mail válido.' })));
            this.done();
          });
        }
        
        break;
      case 'phone':
        skip = false;
        if (message === 'Não enviar telefone')
            skip = true;

        if (this.validatePhone(message) || skip) {
          this.setState({
            lead: {
              ...this.state.lead,
              phone: (skip) ? '' : message
            },
            flow: {
              current: this.state.flow.next
            },
            inputPlaceholder: 'Qual é seu principal problema?'
          });
          
          this.delay(() => {
            this.state.messages.push((new Message({ id: 1, message: 'OK, tudo certo! Nossa equipe vai entrar em contato muito em breve com você!' })));
            this.state.messages.push((new Message({ id: 1, message: 'Você gostaria de falar mais alguma coisa? Algum detalhe sobre a sua necessidade? Pode escrever livremente e repassarei para a equipe.' })));
            this.state.messages.push((new Message({ id: 1, message: 'Lembre-se: Quanto mais você disser, mais teremos como ajudar.' })));
            this.state.messages.push((new Message({ id: 2, message: ['Finalizar'] })));            

            // Descomentar para funcionar o put
            // this.edit();
            this.done();
          });
        } else {
          this.delay(() => {
            this.state.messages.push((new Message({ id: 1, message: 'Por favor, envie um telefone válido.' })));
            this.done();
          });
        }
        break;
      case 'finish':
        skip = false;
        if (message === 'Não enviar telefone')
            skip = true;

        this.setState({
          lead: {
            ...this.state.lead,
            last_question: (skip) ? '' : message
          },
          flow: {
            current: this.state.flow.next,
            next: 'phone'
          },
          inputPlaceholder: 'Mensagem'
        });

        this.delay(() => {
          this.state.messages.push((new Message({ id: 1, message: 'Excelente! Cumprimos nosso objetivo. Prometo que o contato será em breve.' })));
          this.state.messages.push((new Message({ id: 1, message: 'Abraços!' })));          

          // Descomentar para funcionar o put
          // this.edit();
          this.done();
        });
        break;
      default:
        break;
    }
  }

  render() {
    return (
      <div className="chat-vincent" style={styles.body}>
          <ChatFeed
            messages={this.state.messages} // Boolean: list of message objects
            isTyping={this.state.is_typing} // Boolean: is the recipient typing
            handleChange={this.handleChange.bind(this)}
            bubblesCentered={false} //Boolean should the bubbles be centered in the feed?
            handleQuickReply={this.handleQuickReply.bind(this)}
            // JSON: Custom bubble styles
            chatHistoryStyles={ {height: 500} }
            bubbleStyles={
              {
                text: {
                  fontSize: 14
                },
                chatbubble: {
                  borderRadius: 30,
                  padding: '15px 25px',
                  marginLeft: 20,
                  marginRight: 20,
                }
              }
            }
          />
          <div className="chat-input" style={styles.chatInput}>
            <form onSubmit={this.handleSend.bind(this)}>
              <input type="text" value={this.state.message} onChange={this.handleChange.bind(this)} style={styles.inputStyle} placeholder={this.state.inputPlaceholder}></input>
            </form>
          </div>
        </div>
    );
  }
}

export default App;
